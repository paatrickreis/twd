from django.db import models
from category import Category

class Page(models.Model):
	category = models.ForeignKey(Category)
	title = models.CharField(max_length=128)
	url = models.URLField()
	views = models.IntegerField(default=0, blank=True, null=True)

	def __unicode__(self):      #For Python 2, use __str__ on Python 3
		return self.title