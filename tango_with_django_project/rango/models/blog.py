from django.db import models

class Blog(models.Model):
		titulo = models.CharField(max_length=128)
		autor = models.CharField(max_length=128)
		texto = models.TextField()

		def __unicode__(self):
				return self.titulo