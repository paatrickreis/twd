from django.conf.urls import patterns, url
from django.contrib.auth.views import *
from rango.views import *

urlpatterns = patterns('',
        url(r'^$', Index.as_view(), name='index'),
        url(r'^index/$', Index.as_view(), name='index'),
        url(r'^about/$', About.as_view(), name='about'),
        url(r'^add_category/$', AddCategory.as_view(), name='add_category'),
        url(r'^category/(?P<category_name_slug>[\w\-]+)/add_page/$', AddPage.as_view(), name='add_page'),
        url(r'^register/$', RegisterUser.as_view(), name='register'),
        url(r'^category/(?P<category_name_slug>[\w\-]+)/$', ViewCategory.as_view(), name="category"),
    


        url(r'^blog/$', ConsultaBlog.as_view(), name='blog'),
        url(r'^add_noticia/$', AddNoticia.as_view(), name='add_noticia'),
        url(r'^editar_noticia/(?P<blog_id>\d+)/$', AddNoticia.as_view(), name='editar_noticia'),
        url(r'^visualizar_noticia/(?P<blog_id>\d+)/$', VisualizarBlog.as_view(), name='visualizar_noticia'),



        #url(r'^login/$', LogIn.as_view(), name='login'),
        #url(r'^logout/$', LogOut.as_view(), name='logout'),
        #url(r'^restricted/', Restricted.as_view(), name='restricted'),
        url(r'^login/$', 'django.contrib.auth.views.login', {'template_name': 'login2.html'}, name='login'),
        url(r'^logout/$', 'django.contrib.auth.views.logout', {'template_name': 'logout.html'}, name='logout'),

        )