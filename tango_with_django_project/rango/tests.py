# Create your #coding:utf-8
from django.test import TestCase
from django.core.urlresolvers import reverse

from rango.models import Category

class CategoryMethodTests(TestCase):

    def test_ensure_views_are_positive(self):
        cat = Category(name='test')
        cat.save()
        self.assertEqual((cat.views >= 0), True)

    def test_slug_line_creation(self):
        cat = Category(name='Random Category String')
        cat.save()
        self.assertEqual(cat.slug, 'random-category-string')

class PageMethodTests(TestCase):

    def test_insert_page(self):
        pass

class UserProfileMethodTests(TestCase):
    def test_insert_user_profile(self):
        pass

class BlogMethodTests(TestCase):
    def test_send_blog(self):
        pass

class IndexViewTests(TestCase):

    def test_index_view_with_no_categories(self):
        response = self.client.get(reverse('index'))
        self.assertEqual(response.status_code, 200)
        self.assertQuerysetEqual(response.context['categories'], [])
