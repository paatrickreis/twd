# coding: utf-8
from datetime import datetime
from django.http.response import HttpResponseRedirect, HttpResponse
from django.shortcuts import render #, get_object_or_404
from django.views.generic import View
from rango.models import Category, Page
from rango.forms.category import CategoryForm

from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

class ViewCategory(View):

    def get(self, request, category_name_slug=""):
        context = {}

        try:
            context['searched_category'] = category_name_slug
            category = Category.objects.get(slug=category_name_slug)

            set_last_visit = False #controle de views nas categorias
            key = category_name_slug+'_visit'
            if key in request.session:
                last_visit = datetime.strptime(request.session[key][:-7], "%Y-%m-%d %H:%M:%S")
                if (datetime.now() - last_visit).days > 0:
                    set_last_visit = True
            else:
                set_last_visit = True

            if set_last_visit:
                print("Setando visita em %s..." %category_name_slug)
                request.session[key] = str(datetime.now())
                category.views += 1
                category.save()

            context['category'] = category
            pages = Page.objects.filter(category=category).order_by('title')
            context['pages'] = pages
        except:
            return HttpResponseRedirect('/rango/categories/')

        return render(request, 'rango/category.html', context)

class AddCategory(View):

    context = {}

    @method_decorator(login_required)
    def get(self, request):
        form = CategoryForm()
        self.context['form'] = form
        return render(request, 'rango/add_category.html', self.context)

    @method_decorator(login_required)
    def post(self, request):
        form = CategoryForm(request.POST)
        if form.is_valid():
            form.save(commit=True)
            return HttpResponseRedirect('/rango/index/')
        else:
            print(form.errors)
        self.context['form'] = form
        return render(request, 'rango/add_category.html', self.context)

class LikeCategory(View):

    @method_decorator(login_required)
    def get(self, request, category_name_slug=""):
        likes = 0
        try:
            print("Like!")
            category = Category.objects.get(slug=category_name_slug)
            likes = category.likes + 1
            category.likes = likes
            category.save()
        except:
            pass
        return HttpResponse(likes)
        #return HttpResponseRedirect('/rango/category/'+category_name_slug)

class CategoriesList(View):

    def get(self, request):
        context = {}
        context['categories'] = Category.objects.all().order_by('name')
        return render(request, 'rango/categories.html', context)