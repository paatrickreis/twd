#coding: utf-8

from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from django.views.generic import View
from rango.forms.user import UserForm
from rango.forms.user_profile import UserProfileForm
from rango.models import UserProfile

from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

class RegisterUser(View):

    context = {}
    registered = False

    def get(self, request):

        if request.session.test_cookie_worked():
            print ">>>> TEST COOKIE WORKED!"
            request.session.delete_test_cookie()

        user_form = UserForm
        user_profile_form = UserProfileForm
        self.context = {
            'user_form': user_form,
            'user_profile_form': user_profile_form,
            'registered': self.registered
        }
        return render(request, 'rango/register.html', self.context)

    def post(self, request):

        user_form = UserForm(data=request.POST)
        user_profile_form = UserProfileForm(data=request.POST)

        if user_form.is_valid() and user_profile_form.is_valid():
            user = user_form.save()
            user.set_password(user.password)
            user.save()

            profile = user_profile_form.save(commit=False)
            profile.user = user

            if 'picture' in request.FILES:
                print("file exists")
                profile.picture = request.FILES['picture']
            else:
                print("file not exists :(")

            profile.save()
            self.registered = True
        else:
            print(user_form.errors, user_profile_form.errors)

        self.context = {
            'user_form': user_form,
            'user_profile_form': user_profile_form,
            'registered': self.registered
        }
        return render(request, 'rango/register.html', self.context)

class UserSettings(View):

    @method_decorator(login_required)
    def get(self, request):
        return render(request, 'rango/settings.html')
