#coding:utf-8

from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from django.views.generic import View
from rango.models import Page
from rango.forms.page import PageForm

from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

class AddPage(View):

    @method_decorator(login_required)
    def get(self, request):
        context = {}
        if request.method == 'GET':
            form = PageForm()
        context['form'] = form
        return render(request, 'rango/add_page.html', context)

    @method_decorator(login_required)
    def post(self, request):
        context = {}
        form = PageForm(data=request.POST)
        if form.is_valid():
            form.save(commit=True)
            return HttpResponseRedirect('/rango/index/')
        else:
            print(form.errors)
        context['form'] = form
        return render(request, 'rango/add_page.html', context)