#coding:utf-8

from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect, HttpResponse
from django.views.generic import View
from django.shortcuts import render

from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

class LogIn(View):

    def get(self, request):
        return render(request, 'rango/login.html')

    def post(self, request):
        context = {}
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)

        if user:
            if user.is_active:
                login(request, user)
                return  HttpResponseRedirect('/rango/')
            else:
                context['error_message'] = "User is disabled!"
        else:
            context['error_message'] = "username or password are incorrect!"
        return render(request, 'rango/login.html', context)

class LogOut(View):

    @method_decorator(login_required)
    def get(self, request):
        logout(request)
        return HttpResponseRedirect('/rango/')