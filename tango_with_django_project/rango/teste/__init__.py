#coding: utf-8
from rango.views.index import Index
from rango.views.about import About
from rango.views.category import ViewCategory, AddCategory, LikeCategory, CategoriesList
from rango.views.page import AddPage
from rango.views.user import RegisterUser, UserSettings
from rango.views.log import LogIn, LogOut
from rango.views.comment import SendComment
from rango.views.forum import Forum