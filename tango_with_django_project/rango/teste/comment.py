#coding:utf-8
from django.views.generic import View
from django.shortcuts import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

from rango.models import  Category, UserProfile

class SendComment(View):

    @method_decorator(login_required)
    def post(self, request):
        user = request.user
        data = request.POST

        try:
            category = Category.objects.get(slug=data['category'])
            profile = UserProfile.objects.get(user_id=user.id)
            comm = Comment()
            comm.profile = profile
            comm.category = category
            comm.text = data['comment']
            comm.save()
        except:
            pass

        return HttpResponseRedirect('/rango/category/'+data['category']+'/')

