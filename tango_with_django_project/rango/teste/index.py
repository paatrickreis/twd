#coding: utf-8

from django.shortcuts import render
from django.http import response
from rango.models import Category, Page
from django.views.generic import View
from rango.models import UserProfile
from datetime import datetime

class Index(View):

    def get(self, request):
        context = {}
        categories = Category.objects.all().order_by('name')
        context["categories"] = categories
        context["top_liked_categories"] = categories.order_by('-likes')[:5]
        context["top_seen_pages"] = Page.objects.all().order_by('-views')[:5]
        context["recent_pages"] = Page.objects.all().order_by('-id')[:5]
        return render(request, 'rango/index.html', context)