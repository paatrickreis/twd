# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('rango', '0007_auto_20161213_1809'),
    ]

    operations = [
        migrations.CreateModel(
            name='Blog',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('titulo', models.CharField(unique=True, max_length=128)),
                ('autor', models.CharField(max_length=128)),
                ('imagem', models.ImageField(upload_to=b'profile_images', blank=True)),
                ('texto', models.TextField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
