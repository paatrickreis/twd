# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('rango', '0013_auto_20161214_1236'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='blog',
            name='imagem',
        ),
    ]
