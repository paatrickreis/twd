# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('rango', '0010_auto_20161214_0139'),
    ]

    operations = [
        migrations.AlterField(
            model_name='blog',
            name='imagem',
            field=models.ImageField(upload_to=b'/static/imagens/noticia_images', verbose_name=b'Iamgem', blank=True),
        ),
    ]
