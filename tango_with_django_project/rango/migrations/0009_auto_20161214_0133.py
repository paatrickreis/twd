# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('rango', '0008_blog'),
    ]

    operations = [
        migrations.AlterField(
            model_name='blog',
            name='imagem',
            field=models.ImageField(upload_to=b'static/noticia_images', blank=True),
        ),
    ]
