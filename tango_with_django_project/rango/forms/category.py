from django import forms
from rango.models import Category

class CategoryForm(forms.ModelForm):

    name = forms.CharField(
        widget=forms.TextInput(attrs={
            'placeholder': 'Categoria'
        }),
        error_messages={
            'required': 'Campo Obrigatorio'
        }
    )

    class Meta:
        model = Category
        fields = ['name']