# coding: utf-8
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.views.generic import View

class Restricted(View):

	@login_required	
	def post(self,request):
		return HttpResponse("Since you're logged in, you can see this text!")
