# coding: utf-8
from django.views.generic import View
from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth import authenticate, login,logout
from django.contrib.auth.decorators import login_required

class LogIn(View):

	def get(self,request):
		return render(request, 'rango/login.html')

	def post(self, request):

		username = request.POST.get('username')
		password = request.POST.get('password')

		user = authenticate(username=username, password=password)

		if user:
			if user.is_active:
				login(request, user)
				return HttpResponseRedirect(reverse('index'))
			else:
				return HttpResponse("Sua conta no Rango está desabilitada.")
		else:
			mensagem = u'Informações de login inválidas: %s - %s' %(username, password)
			print(mensagem)
			return HttpResponse(mensagem)
		return render(request, 'rango/login.html', {'login': 'login'})

class LogOut(View):

	@login_required
	def user_logout(request):
		logout(request)
		return HttpResponseRedirect('/rango/')
