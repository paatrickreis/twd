# coding: utf-8

from datetime import datetime
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from django.views.generic import View
from rango.models import Page, Category
from rango.forms.page import PageForm

from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

class AddPage(View):

	def get(self, request, category_name_slug):
		print category_name_slug
		try:
			cat = Category.objects.get(slug=category_name_slug)

		except:
			cat = None

		form = PageForm()

		context_dict = {'form':form, 'category': cat}

		return render(request, 'rango/add_page.html', context_dict)

	def post(self,request, category_name_slug):
		print("DOUG E MITAO")
		try:
			cat = Category.objects.get(slug=category_name_slug)
		except:
			cat = None

		context = {}
		form = PageForm(request.POST)
		if form.is_valid():
			if cat:
				page = form.save(commit=False)
				page.category = cat
				page.views = 0
				page.save()
		
				return HttpResponseRedirect('/rango/')
		else:
			print form.errors
		
		context['form'] = form
		return render(request, 'rango/add_page.html', context)
