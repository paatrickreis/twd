# coding: utf-8
from datetime import datetime
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from django.views.generic import View
from rango.models import Category, Page
from rango.forms.category import CategoryForm

from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

class ViewCategory(View):

	def get(self, request, category_name_slug):


		print(category_name_slug)
		context_dict = {}
		context_dict['category_name_slug'] = category_name_slug

		#try:
		category = Category.objects.get(slug=category_name_slug)
		context_dict['category'] = category

		pages = Page.objects.filter(category=category)
		context_dict['pages'] = pages

		#except:
		#	return HttpResponseRedirect('/rango/index/')
	 
		return render(request, 'rango/category.html', context_dict)
			
class AddCategory(View):
	
	context = {}

	@method_decorator(login_required)
	def get(self, request):
		form = CategoryForm()
		self.context['form'] = form
		return render(request, 'rango/add_category.html', self.context)

	@method_decorator(login_required)
	def post(self, request):
		form = CategoryForm(request.POST)
		if form.is_valid():
			form.save(commit=True)
			return HttpResponseRedirect('/rango/')
		else:
			print(form.errors)
		self.context['form'] = form
		return render(request, 'rango/add_category.html', self.context)