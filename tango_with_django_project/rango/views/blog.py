# coding: utf-8
from django.views.generic import View
from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth.decorators import login_required
from rango.models import Blog
from rango.forms.blog import BlogForm
from rango.forms.busca import Busca

class AddNoticia(View):

	template = 'rango/add_noticia_blog.html'

	def get(self, request , blog_id = None):

		if blog_id:
			nome = Blog.objects.get(id =blog_id)
			form = BlogForm(instance= nome)
			editar=True
		else:
			form = BlogForm()
			editar=False

		return render(request, self.template, {'form': form,'editar':editar})

	'''def post(self, request, blog_id=None):
		
		if blog_id:
			nome = Blog.objects.get(id=blog_id)
			form = BlogForm(instance=nome, data=request.POST)
		else:
			print(request.FILES)
			form = BlogForm(request.POST, request.FILES)

		if form.is_valid():
			form.save()
			return HttpResponseRedirect('/rango/blog/')

		else:
			return render(request, self.template, {'form': form})'''

	def post(self, request):
		form = BlogForm(request.POST)
		if form.is_valid():
			form.save(commit=True)
			return HttpResponseRedirect('/rango/')
		else:
			print(form.errors)
		self.context['form'] = form
		return render(request, 'rango/blog.html', self.context)

class ConsultaBlog(View):
	
	template = 'rango/blog.html'
	
	def get(self, request):
		context_dict = {}
		form = Busca()
		blog = Blog.objects.all()
		context_dict['blog'] = blog

		return render(request, self.template, context_dict)
	
	def post(self, request):
		form = Busca(request.POST)
		if form.is_valid():
			blog = Blog.objects.filter(first_name__icontains=form.cleaned_data['nome'])

			return render(request, self.template, {'blog': blog, 'form':form})
		else:
			form = Busca(request.POST)				
			blog = Blog.objects.all()
		return render(request, self.template, {'blog': blog,'form': form})

class VisualizarBlog(View):
	
	template = "rango/visualizar_noticia.html"
	
	def get(self, request, blog_id=None):
		
		if blog_id:
			blog = Blog.objects.get(id=blog_id)
		else:
			return render(request, self.template, { })

		return render(request, self.template, {'blog': blog})
	
	def post(self, request):
		
		return render(request, self.template)