# coding: utf-8
from django.shortcuts import render
from django.views.generic import View

class About(View):

	def get(self, request):
		context = dict(
			autor='Patrick Reis',
			instituicao='Instituto Federal Catarinense - Campus Araquari',
			curso='Bacharelado em Sistemas de Informação (BSI)',
			disciplina='Desenvolvimento Web II',
			professor='Marco André Lopes Mendes')
		return render(request, 'rango/about.html', context)