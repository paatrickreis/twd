# coding: utf-8
from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from rango.models import Category, Page, UserProfile
from datetime import datetime
from django.views.generic import View

class Index(View):

	def get(self, request):
		context_dict = {}

		categories = Category.objects.all().order_by('name')
		context_dict["tudo"] = categories

		category_list = Category.objects.order_by('-likes')[:5] #categorias mais curtidas
		category_tudo = Category.objects
		page_list = Page.objects.order_by('-views')[:5] #páginas mais visitadas

		context_dict['categories'] = category_list
		context_dict['pages'] = page_list

		visits = request.session.get('visits')
		if not visits:
			visits = 1
		reset_last_visit_time = False

		last_visit = request.session.get('last_visit')
		if last_visit:
			last_visit_time = datetime.strptime(last_visit[:-7], "%Y-%m-%d %H:%M:%S")

			if (datetime.now() - last_visit_time).seconds > 30:
				visits = visits + 1
				reset_last_visit_time = True
		else:
			reset_last_visit_time = True

		if reset_last_visit_time:
			request.session['last_visit'] = str(datetime.now())
			request.session['visits'] = visits
		context_dict['visits'] = visits
		context_dict['last_visit'] = last_visit

		response = render(request,'rango/index.html', context_dict)

		return response

