from rango.views.log import LogIn, LogOut
from rango.views.index import Index
from rango.views.about import About
from rango.views.category import AddCategory, ViewCategory
from rango.views.page import AddPage
from rango.views.user import RegisterUser
from rango.views.restricted import Restricted
from rango.views.blog import AddNoticia, ConsultaBlog, VisualizarBlog